#include <PID.h>
#include <VarSpeedServo.h>

// Speed reference, min and max
double lapsRef = 100;
double minLapsRef = 20;
double maxLapsRef = 100;

// PID parameters
const double kP = 3;
const double kI = 0.0003;
const double kD = 10000;

// Pin in out
const unsigned int IREmmiter = 3;
const unsigned int IRDetectorSupply = 4;
const unsigned int IRDetector = 5;
const unsigned int servoPin = 10;

// Time measurment
unsigned long lastPulseTime = millis();
unsigned long halfPeriodTime = millis();
unsigned long enterTime = millis();
unsigned long startServoTime = 0;
unsigned long lastTime;

// Flags
boolean onPendulum = false;
boolean onLeft = true;
boolean started = false;

// Delay from the center position
const int delayOffsetMax = 175;
const int centerDelay = 625; 
const int delayGain = 17;
int currentDelay;
int delayOffset = 0;

// Measure of the half period
const double halfPeriodAverageRatio = 3;
double halfPeriod = 0;
double halfPeriodAverage = 0;

// Proportional and derivative element for regulator
const double lapsAverageRatio = 3;
double laps = 0;
double lapsAverage = 0;

// Regulator output
const unsigned int servoSpeed = 150; 
unsigned int currentSpeed = servoSpeed; 
const double centerPosition = 100;
const double limitStroke = 60;
const double minStroke = 10;
const double maxStroke = 180;
double stroke = 0;
const double strokeAverageRatio = 4;
double strokeAverage = 0;

// Keyboard input
String serialInput = "";

VarSpeedServo myservo;
PID regulator(&lapsAverage, &stroke, &lapsRef, kP, kI, kD, 0, limitStroke, 0, true);

void setup() {
  Serial.begin(9600);

  pinMode(IREmmiter, OUTPUT);
  digitalWrite(IREmmiter, HIGH);

  pinMode(IRDetectorSupply, OUTPUT);
  digitalWrite(IRDetectorSupply, LOW);

  pinMode(IRDetector, INPUT_PULLUP);

  myservo.attach(servoPin);
  stopping();
}

void calcGain (){
  strokeAverage += (stroke - strokeAverage) / strokeAverageRatio;
  double factor = (strokeAverage + 40)/100;
  currentSpeed = servoSpeed * factor;
  double kp = kP * pow(factor, 3);
  double ki = kI * factor;
  double kd = kD * pow(factor, 4);
  Serial.println("kp=" + String(kp) + "\tki=" + String(ki*1000) + "\tkd=" + String(kd/1000) + "\tspeed=" + String(currentSpeed));
  regulator.SetGains(kp, ki, kd);
}

void stopping(){
  stroke = centerPosition;
  myservo.write(centerPosition, 0, false);
  regulator.SetEnabled(false);  
  started = false;
}

void starting(){
  // Begin balancing pendulum
  myservo.write(maxStroke, 0, false);
  delay(centerDelay);
  delay(centerDelay);
  myservo.write(minStroke, 0, false);
  delay(centerDelay);
  delay(centerDelay);
  myservo.write(maxStroke, 0, false);
  delay(centerDelay);
  delay(centerDelay);
  myservo.write(minStroke, 0, false);
  delay(centerDelay);
  delay(centerDelay);
  myservo.write(maxStroke, 0, false);
  delay(centerDelay);
  myservo.write(centerPosition, 0, false);

  // Reset
  unsigned long time = millis();
  lastPulseTime = time;
  halfPeriodTime = time;
  enterTime = time;
  lastTime = 0;
  stroke = 0;
  onLeft = true;
  started = true;
  regulator.SetEnabled(true);
}

void readInput(){
  while (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    if (inChar > 0) {
      switch (inChar) {
      case '\n': // Ignore new-lines
        break;
      case '\r': // Return on CR
        if (serialInput == "stop"){
          stopping();
          Serial.println("System stopped.");
        }
        else if (serialInput == "start"){
          starting();
          Serial.println("System started.");
        }
        else {
          unsigned int ref = serialInput.toInt();
          if (ref <= maxLapsRef && ref >= minLapsRef){
            lapsRef = ref;
            Serial.println("Reference changed to: " + String((int)lapsRef));
          }
        }
        serialInput = "";
        break;
      default:
        serialInput += inChar;
        break;
      }
    }
  }  
}

void loop() {
  if (!started){
    Serial.println("System stopped, send start to initialize the pendulum.");   
    delay(5000); 
    readInput();
    return;
  }

  unsigned long time = millis();

  if (digitalRead(IRDetector) == 1){
    if (time - lastPulseTime > 5 && !onPendulum)
    {
      halfPeriod =  time - halfPeriodTime;
      halfPeriodTime = time;
      enterTime = time;
      onPendulum = true;
    }
  }  
  else{
    if (onPendulum) {
      // Speed calculation
      laps = time - enterTime;
      lapsAverage += (laps - lapsAverage) / lapsAverageRatio;

      // Period calculation
      halfPeriodAverage += (halfPeriod - halfPeriodAverage) / halfPeriodAverageRatio;

      // Delay calculation. Optimizes the delay based on the fact that the pendulum is too fast or too slow.
      delayOffset = (lapsRef - lapsAverage) * delayGain; 
      if (delayOffset > delayOffsetMax){
        delayOffset = delayOffsetMax;
      } 
      else if (delayOffset < -delayOffsetMax) {
        delayOffset = -delayOffsetMax;
      }
      currentDelay = centerDelay + delayOffset;

      // Compute the regulation
      calcGain();
      regulator.Compute();

      // Print values      
      Serial.println("Laps=" + String((int)laps) + "ms\tlapsAverage=" + String((int)lapsAverage) + " ms\tPeriod=" + String((int)(halfPeriodAverage * 2)) + " ms\tStroke=" + String((int)stroke) + "\tDelay=" + String((int)currentDelay));
      startServoTime = time;
      onPendulum = false;

      // Read serial input
      readInput();
    }

    // After the optimized delay is raised, the servo is moved
    if (startServoTime > 0 && (time - startServoTime > currentDelay)){    
      // Move servo
      myservo.write(onLeft ? centerPosition + stroke : centerPosition - stroke, currentSpeed, false);
      onLeft = !onLeft;
      startServoTime = 0;
    }

    lastPulseTime = time;
  }
}









