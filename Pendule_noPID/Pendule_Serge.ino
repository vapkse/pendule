#include <IRremote.h>
#include <IRremoteInt.h>
#include <VarSpeedServo.h>

// Speed reference, min and max
unsigned int lapsRef = 100;
unsigned int minLapsRef = 20;
unsigned int maxLapsRef = 100;

// Pin in out
const unsigned int IRDetectorSupply = 9;
const unsigned int IRDetector = 8;
const unsigned int servoPin = 10;

IRsend remoteSend;
VarSpeedServo myservo;

// Time measurment
unsigned long lastPulseTime = millis();
unsigned long halfPeriodTime = millis();
unsigned long enterTime = millis();
unsigned long startServoTime = 0;
unsigned long lastTime;

// Flags
boolean onPendulum = false;
boolean onLeft = false;
boolean started = false;

// Delay from the center position
const int delayOffsetMax = 175;
const unsigned int centerDelay = 625; 
const unsigned int delayGain = 17;
unsigned int currentDelay;
int delayOffset = 0;

// Measure of the half period
unsigned int halfPeriodAverageRatio = 3;
unsigned long halfPeriod = 0;
float halfPeriodAverage = 0;

// Proportional and derivative element for regulator
const float derivativeGain = 0.18; // progressive, depend of the speed: 0.0018 at 100, 0.009 at 20 -> 0.18/laps
const unsigned int lapsAverageRatio = 3;
unsigned long laps = 0;
float lapsAverage = 0;
float currentGain = derivativeGain / 100;

// Integration element for regulator
const unsigned int accelerationAverageRatio = 3;
const float integralGain = 0.035; 
float acceleration = 0;
float accelerationAverage = 0;
float previousLapsAverage;

// Regulator output
const unsigned int centerPosition = 100;
const unsigned int servoSpeed = 150; 
const float limitStroke = 60;
const unsigned int minStroke = 10;
const unsigned int maxStroke = 180;
float stroke = 0;
unsigned int servoPos;

// Keyboard input
String serialInput = "";

void setup() {
  Serial.begin(9600);

  pinMode(IRDetectorSupply, OUTPUT);
  digitalWrite(IRDetectorSupply, HIGH);

  pinMode(IRDetector, INPUT);

  myservo.attach(servoPin);
  myservo.write(centerPosition, 0, false);

  remoteSend.enableIROut(38);
  remoteSend.mark(0);
}

void moveServo(){  
  // Calc regulator gain extrapolated from the speed  
  currentGain = derivativeGain / lapsAverage;
  
  // Calc regulator stroke
  // Add proportional and derivative elements
  stroke += (lapsAverage - lapsRef)  * currentGain;

  // Add integral element
  stroke -= accelerationAverage * integralGain;

  // Ensure the limits
  if (stroke < -limitStroke){
    stroke = -limitStroke;
  }
  else if (stroke > limitStroke) {
    stroke = limitStroke;
  }
  
  // Calc direction
  servoPos = onLeft ? centerPosition + stroke : centerPosition - stroke;  
  onLeft = !onLeft;

  // Move servo
  myservo.write(servoPos, servoSpeed, false);
}

void stopping(){
  myservo.write(centerPosition, 0, false);
  started = false;
}

void starting(){
  // Begin balancing pendulum
  myservo.write(maxStroke, 0, false);
  delay(centerDelay);
  delay(centerDelay);
  myservo.write(minStroke, 0, false);
  delay(centerDelay);
  delay(centerDelay);
  myservo.write(maxStroke, 0, false);

  // Reset
  unsigned long time = millis();
  lastPulseTime = time;
  halfPeriodTime = time;
  enterTime = time;
  lastTime = 0;
  stroke = 0;
  onLeft = false;
  started = true;
}

void readInput(){
  while (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    if (inChar > 0) {
      switch (inChar) {
      case '\n': // Ignore new-lines
        break;
      case '\r': // Return on CR
        if (serialInput == "stop"){
          stopping();
          Serial.println("System stopped.");
        }
        else if (serialInput == "start"){
          starting();
          Serial.println("System started.");
        }
        else {
          unsigned int ref = serialInput.toInt();
          if (ref <= maxLapsRef && ref >= minLapsRef){
            lapsRef = ref;
            Serial.println("Reference changed to: " + String(lapsRef));
          }
        }
        serialInput = "";
        break;
      default:
        serialInput += inChar;
        break;
      }
    }
  }  
}

void loop() {
  if (!started){
    Serial.println("System stopped, send start to initialize the pendulum.");   
    delay(5000); 
    readInput();
    return;
  }

  unsigned long time = millis();
  unsigned long loopTime = 0;
  if (lastTime > 0){
    loopTime = (millis() - lastTime);
  }
  lastTime = time;

  if (digitalRead(IRDetector) == 1){
    if (time - lastPulseTime > 5 && !onPendulum)
    {
      halfPeriod =  time - halfPeriodTime;
      halfPeriodTime = time;
      enterTime = time;
      onPendulum = true;
    }
  }  
  else{
    if (onPendulum) {
      // Speed calculation
      laps = time - enterTime;
      lapsAverage += (laps - lapsAverage) / lapsAverageRatio;

      // Acceleration calculation
      acceleration = previousLapsAverage - lapsAverage; 
      accelerationAverage += (acceleration - accelerationAverage) / accelerationAverageRatio;      
      previousLapsAverage = lapsAverage;

      // Period calculation
      halfPeriodAverage += (halfPeriod - halfPeriodAverage) / halfPeriodAverageRatio;
      
      // Delay calculation. Optimizes the delay based on the fact that the pendulum is too fast or too slow.
      delayOffset = (lapsRef - lapsAverage) * delayGain; 
      if (delayOffset > delayOffsetMax){
        delayOffset = delayOffsetMax;
      } 
      else if (delayOffset < -delayOffsetMax) {
        delayOffset = -delayOffsetMax;
      }
      currentDelay = centerDelay + delayOffset;

      // Print values      
      Serial.println("Laps=" + String((int)laps) + "ms   lapsAverage=" + String((int)lapsAverage) + " ms   Period=" + String((int)(halfPeriodAverage * 2)) + " ms   Stroke=" + String((int)stroke) +  "   Acceleration=" + String((int)(accelerationAverage * 100))  +  "   Delay=" + String((int)currentDelay) + "    Gain=" + String((int)(currentGain * 1000)) + "      loopTime=" + String(loopTime) + " ms");
      startServoTime = time;
      onPendulum = false;

      // Read serial input
      readInput();
    }

    // After the optimized delay is raised, the servo is moved
    if (startServoTime > 0 && (time - startServoTime > currentDelay)){    
      moveServo();
      startServoTime = 0;
    }

    lastPulseTime = time;
  }
}







