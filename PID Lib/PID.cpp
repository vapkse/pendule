/**********************************************************************************************
 * Arduino PID Library - Version 1.0.0
 * by Da Silva Serge vapkse@gmail.com
 *
 * This Library is licensed under a GPLv3 License
 **********************************************************************************************/

#include "Arduino.h"
#include <PID.h>

/**********************************************************************************************
 * Constructor (...)
 **********************************************************************************************/
PID::PID(double* in, double* out, double* ref, double kp, double ki, double kd, double min, double max, unsigned long time, bool rev)
{
	input = in;
	output = out;
	reference = ref;
	enabled = false;
	reverse = rev;

	PID::SetOutputLimits(min, max);
	PID::SetGains(kp, ki, kd);
	PID::SetSampleTime(time);
}

void PID::SetOutputLimits(double min, double max)
{
	minOutput = min;
	maxOutput = max;

	if (enabled) {
		if (*output > maxOutput) {
			*output = maxOutput;
		}
		else if (*output < minOutput) {
			*output = minOutput;
		};

		if (workingPoint > maxOutput) {
			workingPoint = maxOutput;
		}
		else if (workingPoint < minOutput) {
			workingPoint = minOutput;
		};
	}
}

void PID::SetGains(double kp, double ki, double kd)
{
	if (reverse){
		kP = (0 - kp);
		kI = (0 - ki);
		kD = (0 - kd);
	}
	else {
		kP = kp;
		kI = ki;
		kD = kd;
	}
}

void PID::SetEnabled(bool state)
{
	if (state == !enabled)
	{
		enabled = state;
		if (enabled) {
			PID::Initialize();
		}
	}
}

void PID::SetSampleTime(unsigned long time)
{
	sampleTime = time;
}

void PID::Initialize()
{	
	workingPoint = *output;
	PID::SetOutputLimits(minOutput, maxOutput);
	PID::Compute();
}

bool PID::Compute()
{
	if (!enabled){
		return false;
	}

	unsigned long now = millis();
	if (now > lastTime + sampleTime){
		double kp = kP;
		double ki = kI;
		double kd = kD;

		unsigned long ratio = now - lastTime;
		if (ratio > 0){
			ki *= ratio;
			kd /= ratio;
		}

		Serial.println("kp=" + String(kp) + " ki=" + String(ki) + " kd=" + String(kd) + " wp=" + String(workingPoint));

		// Calc error
		double error = *reference - *input;
		double acceleration = *input - lastInput;
		
		// Add integrale part
		workingPoint += error * ki;

		// Limit working point to the min/max	
		if (workingPoint > maxOutput) {
			workingPoint = maxOutput;
		}
		else if (workingPoint < minOutput) {
			workingPoint = minOutput;
		};

		// Add derivative and proportional parts
		double out = workingPoint + error * kp - acceleration * kd;

		// Limit output point to the min/max	
		if (out > maxOutput) {
			out = maxOutput;
		}
		else if (out < minOutput) {
			out = minOutput;
		};

		*output = out;
		lastTime = now;
		lastInput = *input;
		return true;
	}

	return false;
}

